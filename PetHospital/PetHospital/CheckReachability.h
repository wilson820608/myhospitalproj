//
//  CheckReachability.h
//  PetHospital
//
//  Created by 陳維成 on 2017/1/12.
//  Copyright © 2017年 WilsonChen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "MainMapViewController.h"
@interface CheckReachability : NSObject

+(void)checkReachability:(UIViewController*)viewController;

@end
