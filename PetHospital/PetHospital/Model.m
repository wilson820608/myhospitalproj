//
//  Model.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/28.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import "Model.h"
#import "DataManager.h"
@implementation Model

-(instancetype)initWithData:(NSDictionary*)data{
    
    self = [super init];
    
    if (self) {
        _name = data[@"Name"];
        _lat = [data[@"wgs84aY"] doubleValue];
        _lon = [data[@"wgs84aX"] doubleValue];
        _tel = data[@"Telephone"];
        _address = data[@"Address"];
        _distance = data[@"distance"];
    }
    return self;
    
}
@end
