//
//  MainMapViewController.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/28.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import "MainMapViewController.h"
#import "CheckReachability.h"

@interface MainMapViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>
{
    CLLocationManager* locationManager;
    CLLocation* currentLocation;
    NSArray* data;
    Reachability* serverReach;
    
}
@property (weak, nonatomic) IBOutlet UILabel *remindLabel;
@property (weak, nonatomic) IBOutlet UIButton *locateButton;
@property (weak, nonatomic) IBOutlet MKMapView *mainMapView;
@property (weak, nonatomic) IBOutlet UIView *remindView;
@property (weak, nonatomic) IBOutlet UIButton *callText;

@end

@implementation MainMapViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //remindView
    [self.remindView.layer setMasksToBounds:YES];
    [self.remindView.layer setCornerRadius:10.0f];
    
    self.mainMapView.delegate = self;
    
    //locationManager 設定
    locationManager = [CLLocationManager new];
    [locationManager requestWhenInUseAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest; //精確度
    locationManager.activityType = CLActivityTypeAutomotiveNavigation; //活動種類
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];

    //requestData
    [self requestData];

    self.remindLabel.text = @"定位中";
    [self.callText setEnabled:NO];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // check
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkStatusChanged:) name:kReachabilityChangedNotification object:nil];
    serverReach = [Reachability reachabilityForInternetConnection];
    [serverReach startNotifier];
    [CheckReachability checkReachability:self];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kReachabilityChangedNotification object:nil];
    [serverReach stopNotifier];
}

#pragma mark - updateLocation
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    currentLocation = locations.lastObject;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        self.mainMapView.userTrackingMode = MKUserTrackingModeFollow;
    });
    
    if (data != nil) {
        [self processingDataModel:data];
        [self.callText setEnabled:true];
        self.remindLabel.text = [data[0] objectForKey:@"Name"];
    }
    
    
}

-(void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated{
    
    if(mode == MKUserTrackingModeNone){
        
        UIImage* image1 = [UIImage imageNamed:@"locationWhite.png"];
        [self.locateButton setImage:image1 forState:UIControlStateNormal];
    }
    if(mode == MKUserTrackingModeFollow){
        
        UIImage* image2 = [UIImage imageNamed:@"location.png"];
        [self.locateButton setImage:image2 forState:UIControlStateNormal];
    }
}
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    if (annotation == mapView.userLocation) {
        return nil;
    }
    NSString* hospitalAnnitatinID = @"hospital";
    
    MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"hospital"];
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:hospitalAnnitatinID];
    }else{
        annotationView.annotation = annotation;
    }
    annotationView.canShowCallout = true;
    
    UIImage* showOut = [UIImage imageNamed:@"showOut.png"];
    UIImage* image = [UIImage imageNamed:@"angelAnnta.png"];
    annotationView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:showOut];
    
    annotationView.image = image;
    
    return annotationView;
    
}

#pragma mark - IBAction

- (IBAction)settingButton:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"gotoSettingPage" sender:nil];
}

- (IBAction)searchButton:(UIBarButtonItem *)sender {
    
    [self performSegueWithIdentifier:@"gotoSearchPage" sender:nil];
}

- (IBAction)mainCallphone:(UIButton *)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"電話" message:[data[0] objectForKey:@"Telephone"] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* call = [UIAlertAction actionWithTitle:@"撥打" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString* stringURLContent = [NSString stringWithFormat:@"Tel:%@",[data[0] objectForKey:@"Telephone"]];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[stringURLContent stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:cancel];
    [alert addAction:call];
    
    
    [self presentViewController:alert animated:true completion:nil];
}


- (IBAction)userLocation:(UIButton *)sender {
    self.mainMapView.userTrackingMode = MKUserTrackingModeFollow;
}


#pragma mark - Methods
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"gotoSearchPage"]){
        [CheckReachability checkReachability:self];
        //[self checkLocationAllow];
        ListViewController* viewController = segue.destinationViewController;
        viewController.records = data;
    }
    
}


-(void)addannotation{
    
    for (NSDictionary* dic in data) {
        
        Model* downloadModel = [[Model alloc]initWithData:dic];
        
        CLLocationCoordinate2D coordinate ;
        
        coordinate.latitude = downloadModel.lat;
        coordinate.longitude = downloadModel.lon;
        
        // Add a annotation to mainMapView
        MKPointAnnotation* annottation = [MKPointAnnotation new];
        annottation.coordinate = coordinate;
        annottation.title = downloadModel.name;
        annottation.subtitle = downloadModel.address;
        
        
        [self.mainMapView addAnnotation:annottation];
    }
}
-(void)processingDataModel:(NSArray*)dataModel{
    
    NSArray *sortedDistanceArray = [NSArray new];
    NSMutableArray* distanceArray= [NSMutableArray new];

    for (int i=0; i<dataModel.count; i++) {
        
        Model* model = [[Model alloc]initWithData:dataModel[i]];
        
        CLLocation* hospitalLocation = [[CLLocation alloc]initWithLatitude:model.lat longitude:model.lon];
        CLLocationDistance meter = [currentLocation distanceFromLocation:hospitalLocation];
        NSString* meterStr = [NSString stringWithFormat:@"%0.2f",meter/1000];

        //把distance 加進去
        NSMutableDictionary * distance = [NSMutableDictionary dictionaryWithDictionary:dataModel[i]];
        distance[@"distance"] = meterStr;
        [distanceArray addObject:distance];
        
        distance = [NSMutableDictionary new];
    }
    
    
    //排序
    sortedDistanceArray = [distanceArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        NSDictionary* a = obj1;
        double aa = [[a objectForKey:@"distance"] doubleValue];
        NSDictionary* b = obj2;
        double bb = [[b objectForKey:@"distance"] doubleValue];
        
        return aa < bb ? NSOrderedAscending : NSOrderedDescending;
    }];
    
    data = sortedDistanceArray;
    [self.callText setEnabled:true];
    self.remindLabel.text = [data[0] objectForKey:@"Name"];
    
}

-(void)checkLocationAllow{
    
    BOOL isChecklocation = [CLLocationManager locationServicesEnabled];
    if (isChecklocation)
    {
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            self.remindLabel.text = @"定位中";
            [self.callText setEnabled:false];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"尚未允許" message:@"前往設定開啟定位" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:cancel];
            [alert addAction:ok];
            
            
            [self presentViewController:alert animated:true completion:nil];
            
        }
    }
    
}
-(void) networkStatusChanged:(NSNotification*)notify{
    
    
    NetworkStatus status = serverReach.currentReachabilityStatus;
    
    if (status == NotReachable) {
        
    }else{
        [self requestData];
    }
    
    
}

-(void) requestData{
    [DataManager dataConnectWithCompletion:^(NSError *theError, NSArray *theResult) {
        if (theError) {
            NSLog(@"%@",theError);
            return;
        }
        
        [self.mainMapView removeAnnotations:self.mainMapView.annotations];
        data = theResult;
        [self addannotation];
        [self processingDataModel:data];
        [self checkLocationAllow];
    }];
}


@end
