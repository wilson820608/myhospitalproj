//
//  detailViewController.h
//  PetHospital
//
//  Created by 陳維成 on 2016/12/13.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MainMapViewController.h"
#import "Model.h"
@interface detailViewController : UIViewController

@property (nonatomic,strong) NSDictionary * detailDict;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;



@end
