//
//  CheckReachability.m
//  PetHospital
//
//  Created by 陳維成 on 2017/1/12.
//  Copyright © 2017年 WilsonChen. All rights reserved.
//

#import "CheckReachability.h"

@implementation CheckReachability
+(void)checkReachability:(UIViewController*)viewController{
    
    Reachability* serverReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = serverReach.currentReachabilityStatus;
    
    if (status == NotReachable) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"偵測不到網路" message:@"請確定網路功能開啟" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:nil];
        
        
        
        [alert addAction:ok];
        
        
        [viewController presentViewController:alert animated:true completion:nil];
        
    }
}
@end
