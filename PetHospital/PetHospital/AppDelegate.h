//
//  AppDelegate.h
//  PetHospital
//
//  Created by 陳維成 on 2016/12/12.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

