//
//  CellButton.h
//  PetHospital
//
//  Created by 陳維成 on 2016/12/13.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellButton : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *distance;

@end
