//
//  detailViewController.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/13.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import "detailViewController.h"

@interface detailViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIButton *cellphoneBTN;
@property (weak, nonatomic) IBOutlet UIButton *navigationButton;


@end

@implementation detailViewController
{
    Model* dataModel;
}

- (void)viewDidLoad {
    
    UIColor * bgColor = [[UIColor alloc]initWithRed:34.0/255.0 green:58.0/255.0 blue:68.0/255.0 alpha:1.0];
    
    self.detailView.backgroundColor  = UIColor.blackColor;
    // 123321
    

    
    [super viewDidLoad];
    self.navigationItem.title = @"詳細資料";
    self.mapView.delegate = self;
    

    
    dataModel = [[Model alloc]initWithData:_detailDict];
    self.titleLabel.text = dataModel.name;
    self.addressLabel.text = dataModel.address;
  
    [self customs:_cellphoneBTN];
    [self customs:_navigationButton];
    

    CLLocationCoordinate2D coordinate;

    coordinate.latitude = dataModel.lat;
    coordinate.longitude = dataModel.lon;
    
    //目前的縮放大小 上下螢幕距離
    MKCoordinateSpan span = MKCoordinateSpanMake(0.001,0.001);
    // 前面是中心點 後面是範圍
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
    
    
    [self.mapView setRegion:region animated:NO];
    MKPointAnnotation* annottation = [MKPointAnnotation new];
    annottation.coordinate = coordinate;
    annottation.title = dataModel.name;
    annottation.subtitle = dataModel.address;
    
    [self.mapView addAnnotation:annottation];
    
}
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    if (annotation == mapView.userLocation) {
        return nil;
    }
    NSString* hospitalAnnitatinID = @"hospital";
    
    MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"hospital"];
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:hospitalAnnitatinID];
    }else{
        annotationView.annotation = annotation;
    }
    annotationView.canShowCallout = true;
    
    UIImage* showOut = [UIImage imageNamed:@"showOut.png"];
    UIImage* image = [UIImage imageNamed:@"angelAnnta.png"];
    annotationView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:showOut];
    
    annotationView.image = image;
    
    return annotationView;
    
}
#pragma mark - IBAction
- (IBAction)callphone:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"電話" message:dataModel.tel preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* call = [UIAlertAction actionWithTitle:@"撥打" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString* stringURLContent = [NSString stringWithFormat:@"Tel:%@",dataModel.tel];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[stringURLContent stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:cancel];
    [alert addAction:call];
    
    [self presentViewController:alert animated:true completion:nil];
}

- (IBAction)navigationButton:(UIButton *)sender {
    

    MKMapItem* sourceMapItem = [MKMapItem mapItemForCurrentLocation];
    CLLocationCoordinate2D targetCoordinate = CLLocationCoordinate2DMake(dataModel.lat, dataModel.lon);
    MKPlacemark* targetPlace = [[MKPlacemark alloc]initWithCoordinate:targetCoordinate addressDictionary:nil];
    MKMapItem* targetMapItem = [[MKMapItem alloc] initWithPlacemark:targetPlace];
    targetMapItem.name = dataModel.name;
    
    NSDictionary* option = @{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving};
    
    [MKMapItem openMapsWithItems:@[sourceMapItem,targetMapItem] launchOptions:option];
   
    
}

-(void)customs:(UIButton*)button{
    
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:6.0f];
    [button.layer setBorderColor:[UIColor whiteColor].CGColor];
    [button.layer setBorderWidth:2.0];
    
}

@end
