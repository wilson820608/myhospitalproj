//
//  main.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/12.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
