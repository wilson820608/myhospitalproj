//
//  SettingTableViewController.m
//  PetHospital
//
//  Created by 陳維成 on 2017/1/11.
//  Copyright © 2017年 WilsonChen. All rights reserved.
//

#import "SettingTableViewController.h"
#import <Social/Social.h>
//要import才能用 MFMailComposeViewController
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
@interface SettingTableViewController ()<MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *settingTableView;
@property (weak, nonatomic) IBOutlet UILabel *checkLocation;
@property (weak, nonatomic) IBOutlet UILabel *checkLocationAllow;
@property (weak, nonatomic) IBOutlet UILabel *checkInternet;

@end

@implementation SettingTableViewController
{
    
    Reachability* serverReach;
    NetworkStatus status;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.tintColor =[UIColor whiteColor];
    
    [self setLabelStatus];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setLabelStatus) name:@"BecomeActive" object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"BecomeActive" object:nil];
}
-(void)setLabelStatus{
    serverReach = [Reachability reachabilityForInternetConnection];
    status = serverReach.currentReachabilityStatus;
    
    if (![CLLocationManager locationServicesEnabled]) {
        self.checkLocation.text = @"OFF";
    }else{
        self.checkLocation.text = @"ON";
    }
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        self.checkLocationAllow.text = @"OFF";
    }else{
        self.checkLocationAllow.text = @"ON";
    }
    if(status == NotReachable){
        self.checkInternet.text = @"OFF";
    }else{
        self.checkInternet.text = @"ON";
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"分享" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* line = [UIAlertAction actionWithTitle:@"分享到 Line" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self shareToLine];
        }];
        UIAlertAction* facebook = [UIAlertAction actionWithTitle:@"分享到 Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self shareToFacebook];
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:line];
        [alert addAction:facebook];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if(indexPath.section == 1 && indexPath.row == 1){
        [self sendEMail];
    }else if(indexPath.section == 0 && indexPath.row == 1){
        [self gotoSettingLocation];
    }else if(indexPath.section == 0 && indexPath.row == 2){
        [self gotoSettingInternet];
    }else if(indexPath.section == 0 && indexPath.row == 0){
        [self goToLocationServicesSetting];
    }
    
}
#pragma mark - shareApp & send E-mail Methods
-(void)shareToLine {
    //新寫法 轉utf8的意思

    NSURL* lineURL = [NSURL URLWithString:[@"line://msg/text/NewTaipeiCity Hospital on AppStore\n 新北市醫院 \nhttps://itunes.apple.com/us/app/id1195422767" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    if ([[UIApplication sharedApplication] canOpenURL: lineURL]) {
        [[UIApplication sharedApplication] openURL: lineURL];
    }
    else {
        NSURL *itunesURL = [NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id443904275"];
        [[UIApplication sharedApplication] openURL:itunesURL];
    }
}


- (void)shareToFacebook {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController* viewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];

        [viewController addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/id1195422767"]];
        viewController.popoverPresentationController.sourceView = self.view;
        
        [self presentViewController:viewController animated:true completion:nil];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"You need to do is set the Facebook settings on Setting App." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:true completion:nil];
        
    }
}

- (void)sendEMail {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mail = [MFMailComposeViewController new];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"意見 關於 新北市醫院"];
        [mail setMessageBody:@"" isHTML:NO];
        [mail setToRecipients:@[@"wilson820608@gmail.com"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
}
-(void)goToLocationServicesSetting{
    if (![CLLocationManager locationServicesEnabled]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"尚未開啟" message:@"前往設定開啟定位" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            double version = [[UIDevice currentDevice].systemVersion doubleValue];
            if (version >= 10) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=LOCATION_SERVICES"]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
            }
            
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
        
        
        [alert addAction:cancel];
        [alert addAction:ok];
        
        
        [self presentViewController:alert animated:true completion:nil];
    }
}
-(void)gotoSettingLocation{
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"尚未允許" message:@"前往設定開啟定位" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
        
        
        [alert addAction:cancel];
        [alert addAction:ok];
        
        
        [self presentViewController:alert animated:true completion:nil];
    }
    
}
-(void)gotoSettingInternet{
    
    if (status == NotReachable){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"尚未允許" message:@"前往設定網路" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
        
        
        [alert addAction:cancel];
        [alert addAction:ok];
        
        
        [self presentViewController:alert animated:true completion:nil];
    }
    
}

#pragma mark - MFMailComposeViewControllerDelegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        if (error || (result == MFMailComposeResultFailed)) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"錯誤發生 請稍候再試" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:true completion:nil];
            return;
        }
        if ((result == MFMailComposeResultSent)) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"成功" message:@"Mail 成功寄出" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:true completion:nil];
        }
        
    }];
    
}

@end
