//
//  ViewController.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/12.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import "ListViewController.h"
@interface ListViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchResultsUpdating>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) UISearchController * searchController;
@property(strong,nonatomic) UIView* tableViewBgc;

@end

@implementation ListViewController
{
    
    DataManager* connect;
    Model* dataModel;
    CAGradientLayer* gradient;
    NSMutableArray* filteredContentList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //navigationController 設定
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.tintColor =[UIColor whiteColor];
    
    //tableView 設定
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundView = self.tableViewBgc;
    //self.view.bounds.size.width*0.2 可以找到你要比對的
    self.tableView.separatorInset = UIEdgeInsetsMake(0.0,self.view.bounds.size.width*0.2, 0.0, 0.0);
    
    
    filteredContentList = [[NSMutableArray alloc]init];
}

#pragma mark - lazy init

-(UISearchController*)searchController{
    if (!_searchController) {
        // searchController 設定
        _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        _searchController.searchResultsUpdater = self;
        _searchController.searchBar.delegate = self;
        self.definesPresentationContext = true;
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        _searchController.searchBar.frame = CGRectMake(_searchController.searchBar.frame.origin.x, _searchController.searchBar.frame.origin.y, _searchController.searchBar.frame.size.width, 44.0);
        _searchController.searchBar.barTintColor = [UIColor colorWithRed:41.0/255.0 green:152.0/255.0 blue:131.0/255.0 alpha:0.5];
        _searchController.searchBar.tintColor = [UIColor whiteColor];
    }
    return _searchController;
}

-(UIView*)tableViewBgc{
    //設定 UIView漸層色
    if (!_tableViewBgc) {
        _tableViewBgc = [[UIView alloc]init];
        UIColor * one = [[UIColor alloc]initWithRed:60.0/255.0 green:85.0/255.0 blue:94.0/255.0 alpha:1.0];
        UIColor * two = [[UIColor alloc]initWithRed:29.0/255.0 green:29.0/255.0 blue:29.0/255.0 alpha:1.0];
        gradient = [CAGradientLayer new];
        gradient.colors = @[(id)one.CGColor,(id)two.CGColor];
        [_tableViewBgc.layer insertSublayer:gradient atIndex:0];
    }
    
    return _tableViewBgc;
}


#pragma mark - tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (_searchController.isActive && ![_searchController.searchBar.text isEqualToString:@""]){
        return filteredContentList.count;
    }else{
        return _records.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellButton* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //cellBtn 樣子
    cell.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    cell.contentView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    
    //關起來被選到會有顏色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (_searchController.isActive && ![_searchController.searchBar.text isEqualToString:@""]){
        
        NSDictionary * dic = filteredContentList[indexPath.row];
        dataModel = [[Model alloc]initWithData:dic];
        cell.title.text = dataModel.name;
        cell.address.text = dataModel.address;
        
        if ([dataModel.distance doubleValue]>100.0) {
            cell.distance.text = [NSString stringWithFormat:@"極遠"];
        }else{
            cell.distance.text = [NSString stringWithFormat:@"%@ km",dataModel.distance];
        }
        
    }else{
        NSDictionary* dic = _records[indexPath.row];
        dataModel = [[Model alloc]initWithData:dic];
        cell.title.text = dataModel.name;
        cell.address.text = dataModel.address;
        
        if ([dataModel.distance doubleValue]>100.0) {
            cell.distance.text = [NSString stringWithFormat:@"極遠"];
        }else{
            cell.distance.text = [NSString stringWithFormat:@"%@ km",dataModel.distance];
        }
    }
    return cell ;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    detailViewController* detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
    
    if (filteredContentList.count > 0) {
        NSDictionary* sendToDetailView = filteredContentList[indexPath.row];
        detailVC.detailDict = sendToDetailView;
    }else{
        NSDictionary* sendToDetailView = _records[indexPath.row];
        detailVC.detailDict = sendToDetailView;
    }
    
    [self showViewController:detailVC sender:self];
   
}
#pragma mark - UISearchResultsUpdating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    [filteredContentList removeAllObjects];
    
    NSString* searchString = searchController.searchBar.text;
    
    for (int i = 0; i < _records.count; i++) {
        
        NSDictionary * eachDict = _records[i];
        
        NSString* titleString = eachDict[@"Name"];
        NSString* addressString = eachDict[@"Address"];
        
        if ([titleString rangeOfString:searchString].location != NSNotFound || [addressString rangeOfString:searchString].location != NSNotFound) {
            
            [filteredContentList addObject:eachDict];
            
        }
        
    }
    
    [self.tableView reloadData];
}

#pragma mark - Methods
-(void)setBackgroundViewColor:(UIView*)view{
    //設定 UIView漸層色
    UIColor * one = [[UIColor alloc]initWithRed:60.0/255.0 green:85.0/255.0 blue:94.0/255.0 alpha:1.0];
    UIColor * two = [[UIColor alloc]initWithRed:29.0/255.0 green:29.0/255.0 blue:29.0/255.0 alpha:1.0];
    gradient = [CAGradientLayer new];
    gradient.colors = @[(id)one.CGColor,(id)two.CGColor];
    [view.layer insertSublayer:gradient atIndex:0];
}


-(void)viewWillLayoutSubviews{
    //resize view
    [super viewWillLayoutSubviews];
    gradient.frame = self.view.frame;
}

@end
