//
//  Internet.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/13.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import "DataManager.h"


@implementation DataManager
+(void)dataConnectWithCompletion:(CompletionHandle)completion;{
    
    NSString* urlString = @"http://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-002779-001";
    NSURL* url = [NSURL URLWithString:urlString];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionTask* task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"stop");
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(error,nil);
            });
            return;
        }
        
        NSError* theError;
        
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&theError];
        
        NSDictionary * recordData = dataDict[@"result"];
        
        NSArray* dataArray = recordData[@"records"];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil,dataArray);
        });
        
       
        
        
    }];
    
    [task resume];
    
}



@end
