//
//  ViewController.h
//  PetHospital
//
//  Created by 陳維成 on 2016/12/12.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "CellButton.h"
#import "detailViewController.h"
#import "Model.h"

@interface ListViewController : UIViewController
@property NSArray* records;
@end

