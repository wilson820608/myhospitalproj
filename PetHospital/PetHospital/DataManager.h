//
//  Internet.h
//  PetHospital
//
//  Created by 陳維成 on 2016/12/13.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

typedef void(^CompletionHandle)(NSError *theError ,NSArray* theResult);

//@property NSArray * data;

+(void)dataConnectWithCompletion:(CompletionHandle)completion;

@end
