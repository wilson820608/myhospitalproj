//
//  Model.h
//  PetHospital
//
//  Created by 陳維成 on 2016/12/28.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject
@property NSString* name;
@property double lat;
@property double lon;
@property NSString* tel;
@property NSString* address;
@property NSString* distance;
-(instancetype)initWithData:(NSDictionary*)data;
@end
