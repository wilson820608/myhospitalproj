//
//  CellButton.m
//  PetHospital
//
//  Created by 陳維成 on 2016/12/13.
//  Copyright © 2016年 WilsonChen. All rights reserved.
//

#import "CellButton.h"

@implementation CellButton

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    [_distance.layer setMasksToBounds:YES];
    [_distance.layer setCornerRadius:10.0f];
    _distance.backgroundColor = [UIColor colorWithWhite:187.0/255.0 alpha:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
